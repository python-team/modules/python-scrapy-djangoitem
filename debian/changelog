python-scrapy-djangoitem (1.1.1-6) unstable; urgency=medium

  * Team Upload
  * Trim autopkgtest deps
  * Use dh-sequence-python3
  * Patch-out usage of python3-six

 -- Alexandre Detiste <tchet@debian.org>  Sun, 12 Jan 2025 15:32:02 +0100

python-scrapy-djangoitem (1.1.1-5) unstable; urgency=low

  [ Ondřej Nový ]
  * Bump Standards-Version to 4.4.1.
  * d/control: Update Maintainer field with new Debian Python Team
    contact address.
  * d/control: Update Vcs-* fields with new Debian Python Team Salsa
    layout.

  [ Debian Janitor ]
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository,
    Repository-Browse.
  * Update standards version to 4.5.0, no changes needed.

  [ Michael Fladischer ]
  * Update d/watch to work with github again.
  * Bump debhelper version to 13.
  * Bump Standards-Version to 4.6.0.1.
  * Update year in d/copyright.
  * Use uscan version 4.
  * Set Rules-Requires-Root: no.
  * Enable upstream testsuite for autopkgtests.

 -- Michael Fladischer <fladi@debian.org>  Mon, 31 Jan 2022 18:38:32 +0000

python-scrapy-djangoitem (1.1.1-4) unstable; urgency=medium

  * Team upload.
  * Reupload without binaries to allow migration.

 -- Andrey Rahmatullin <wrar@debian.org>  Sun, 04 Aug 2019 21:30:17 +0500

python-scrapy-djangoitem (1.1.1-3) unstable; urgency=low

  * Bump debhelper compatibility and version to 12 and switch to
    debhelper-compat.
  * Bump Standards-Version to 4.4.0.
  * Remove Python2 support.

 -- Michael Fladischer <fladi@debian.org>  Fri, 26 Jul 2019 10:56:43 +0200

python-scrapy-djangoitem (1.1.1-2) unstable; urgency=medium

  [ Ondřej Nový ]
  * d/control: Set Vcs-* to salsa.debian.org
  * Refresh patches after git-dpm to gbp pq conversion

  [ Michael Fladischer ]
  * Run wrap-and-sort -bast to reduce diff size of future changes.
  * Bump debhelper compatibility and version to 11.
  * Bump Standards-Version to 4.1.4.
  * Add python(3)-django and python(3)-scrapy to Depends (Closes:
    #896364, #896255).
  * Remove X-Python(3)-Version field as it is no longer applicable.
  * Enable autopkgtest-pkg-python testsuite.

 -- Michael Fladischer <fladi@debian.org>  Sun, 06 May 2018 20:34:30 +0200

python-scrapy-djangoitem (1.1.1-1) unstable; urgency=low

  * Initial release (Closes: #853985).

 -- Michael Fladischer <fladi@debian.org>  Thu, 02 Feb 2017 20:07:26 +0100
